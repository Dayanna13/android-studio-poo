package dayi.pm.coveña.poo.datalevel;


public interface GetCallback<DataObject> {
    public void done(DataObject object, DataException e);
}
